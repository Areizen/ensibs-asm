.386
.model flat,stdcall
option casemap:none

include c:\masm32\include\windows.inc
include c:\masm32\include\gdi32.inc
include c:\masm32\include\gdiplus.inc
include c:\masm32\include\user32.inc
include c:\masm32\include\kernel32.inc
include c:\masm32\include\msvcrt.inc

includelib c:\masm32\lib\gdi32.lib
includelib c:\masm32\lib\kernel32.lib
includelib c:\masm32\lib\user32.lib
includelib c:\masm32\lib\msvcrt.lib

.data
  message1 db"Un des divseurs de %d est %d",10,0
  message db "Veuillez entrer un entier:",10,0
  scanf db "%d",0
  message2 db"La factoriel de %d est %d",10,0


.data?
  value dword ?

.code

  all_diviseur:
    ; sauvegarde de l'adresse de retour
    push ebp
    mov ebp,esp
    sub esp,4

    mov ecx,1
    for_1:
      mov ebx,ecx
      mov eax,[ebp+8]
      xor edx,edx
      div ebx
      test edx,edx
      mov [ebp-4],ecx
      jne end_for
      push ebx
      push [ebp+8]
      push offset message1
      call crt_printf
      end_for:
        mov ecx,[ebp-4]
        inc ecx
        cmp ecx,[ebp+8]
        jle for_1

    mov esp,ebp
    pop ebp
    ret

  factoriel:
    push ebp
    mov ebp,esp

    mov ebx,[ebp+8]
    cmp ebx,1
    jne calc
      mov eax,1
      jmp end_facto
    calc:
      dec ebx
      push ebx
      call factoriel
      xor edx,edx
      mov ebx,[ebp+8]
      mul ebx
    end_facto:
    mov esp,ebp
    pop ebp
    ret

  start:
    push offset message
    call crt_printf

    push offset value
    push offset scanf
    call crt_scanf

    push dword ptr value
    call all_diviseur

    push dword ptr value
    call factoriel

    push eax
    push dword ptr value
    push offset message2
    call crt_printf

    push 0
    call ExitProcess
  end start
