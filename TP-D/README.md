# Rapport de TP-D ENSIBS-ASM #

Auteur : Romain KRAFT <romain.kraft@protonmail.com>



## Première partie : Calcule des diviseurs ##

### Pseudo Code:

```
fonction all_diviseur(ENTIER n)
  début
    pour i de 0 a n + 1:
      si n % i == 0 :
        afficher i
  fin
```

### Code ASM : ###
```
all_diviseur:
  ; sauvegarde de l'adresse de retour
  push ebp
  mov ebp,esp
  sub esp,4

  mov ecx,1
  for_1:
    mov ebx,ecx
    mov eax,[ebp+8]
    xor edx,edx
    div ebx
    test edx,edx
    mov [ebp-4],ecx
    jne end_for
    push ebx
    push [ebp+8]
    push offset message1
    call crt_printf
    end_for:
      mov ecx,[ebp-4]
      inc ecx
      cmp ecx,[ebp+8]
      jle for_1

  mov esp,ebp
  pop ebp
  ret
```

### Explications: ###

Ici pas besoin d'appeler de variables locales, la seule instruction "exotiques" (car nous l'avons jamais utilisée avant) est `div`.

En effet, `div` va diviser `eax` par la valeur qu'on lui passe ( ici `ebx` ), stocker le quotient dans `eax` et le reste dans `edx` ( à noter le `xor edx,edx` avant la division sans quoi on obtient un `INT_OVERFLOW`)


## Seconde partie : Factoriel récursif ##

### Pseudo Code : ###

```c
fonction factoriel(ENTIER n):ENTIER
  début
    si n == 1 :
      retourne 1
    sinon
      retourne n * factoriel( n - 1 )
  fin
```

### Code ASM : ###
```masm32
factoriel:
  push ebp
  mov ebp,esp

  mov ebx,[ebp+8]
  cmp ebx,1
  jne calc
    mov eax,1
    jmp end_facto
  calc:
    dec ebx
    push ebx
    call factoriel
    xor edx,edx
    mov ebx,[ebp+8]
    mul ebx
  end_facto:
  mov esp,ebp
  pop ebp
  ret
```

### Explications : ###

Ici le point différent aux autres TP est qu'on appelle la fonction `factoriel`  de façon récursive.

La stack ressemble donc à la suivante :

<center>
  <img width="60%" src="./assets/stack_recursif.png"/>
</center>
