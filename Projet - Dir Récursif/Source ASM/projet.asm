.386
.model flat,stdcall
option casemap:none

include c:\masm32\include\windows.inc
include c:\masm32\include\gdi32.inc
include c:\masm32\include\gdiplus.inc
include c:\masm32\include\user32.inc
include c:\masm32\include\kernel32.inc
include c:\masm32\include\msvcrt.inc

includelib c:\masm32\lib\gdi32.lib
includelib c:\masm32\lib\kernel32.lib
includelib c:\masm32\lib\user32.lib
includelib c:\masm32\lib\msvcrt.lib


; définition des chaines et des variables constantes
.data
  ; données définies par des #define dans l'implémentation en C
  chemin db"Veuillez entrer un chemin :",10,0
  max_path dword 4096
  prefix  db 9,0
  ;initial_path db"C:\Users\Reverse\Desktop\x96 dbg",0
  slash_joker db "\*",0
  slash db "\",0
  dot db".",0
  double_dot db"..",0
  string db"%s",0

  ; commandes
  command db "PAUSE",0

  ; messages de format
  error_handle db "Erreur lors de l'exécution de FindFirstFile",10,0
  directory_print db "%sDirectory : %s",10,0
  file_print db "%sFile : %s",10,0

; definition des variables non initialisées

.data?

  initial_path db ?

.code

  ; dir_rec
  dir_rec:
    ; on sauvegarde l'adresse de retour
    push ebp
    mov ebp,esp

    ; on alloue 8 de mémoire pour les variables locales
    ; - 4 octets pour HANDLE
    ; - 592 octets pour WIN32_FIND_DATA
    ; - 4096 pour next_path
    ; - 4096 pour next_prefix
    ; - 4096 pour path_with_joker

    ; on a donc les variables aux adresse suivantes :
    ; &handle => ebp - 12884
    ; &find_data => ebp - 12880
    ; next_path => ebp - 12288
    ; next_prefix => ebp - 8192
    ; path_with_joker => ebp - 4096
    sub esp, 12884

    ;-------------------------------------------------- On créé le path_avec le joker
    mov esi, [ebp + 8]
    push esi

    ;	wcscpy_s(path_with_joker,path);
    mov eax,ebp
    sub eax,4096
    push esi
    push eax
    call crt_strcpy


    ;wcscat_s(path_with_joker, L"\\*");
    mov eax,ebp
    sub eax,4096
    push offset slash_joker
    push eax
    call crt_strcat


    ;-------------------------------------------------- on appelle le FinFirstFile
    ;on stock path with joker
    mov eax,ebp
    sub eax,4096

    ; on met le find_data dans ebp
    mov ebx,ebp
    sub ebx,12880

    ;handle = FindFirstFile(path_with_joker,&find_data)
    push ebx
    push eax
    call FindFirstFile
    mov [ebp - 12884],eax

    ;----------------------------------------------Condidition
    ;	if (handle == INVALID_HANDLE_VALUE) {
    cmp eax, -1
    jne handle_ok

    ; message erreur
    push offset error_handle
    ; wprintf(L"Erreur lors de l'execution de FindFirstFile(\"%ws\",0x%p) ==> Erreur Code : %d\n", path, &find_data,GetLastError());
    call crt_printf

    jmp return
    ;else{
    handle_ok:

    ; DWORD    dwFileAttributes;   0
    ; FILETIME ftCreationTime;     4
    ; FILETIME ftLastAccessTime;   12
    ; FILETIME ftLastWriteTime;    20
    ; DWORD    nFileSizeHigh;      28
    ; DWORD    nFileSizeLow;       32
    ; DWORD    dwReserved0;        36
    ; DWORD    dwReserved1;        40
    ; TCHAR    cFileName[MAX_PATH];44
    ; TCHAR    cAlternateFileName[14];
    do_while:
    mov ebx,ebp
    sub ebx,12880
    mov ebx,[ebx]
    cmp ebx,16
    jne is_file

    mov ebx,ebp
    sub ebx,12880
    add ebx,44
    push ebx

    push [ebp + 12]

    push offset directory_print
    call crt_printf

    ;	if (wcsncmp(find_data.cFileName, L".", MAXPATH) != 0 && wcsncmp(find_data.cFileName, L"..", MAXPATH)) {
    mov ebx,ebp
    sub ebx,12880
    add ebx,44
    push ebx
    push offset dot
    call crt_strcmp
    test eax,eax
    je do_while_end

    mov ebx,ebp
    sub ebx,12880
    add ebx,44
    push ebx
    push offset double_dot
    call crt_strcmp
    test eax,eax
    je do_while_end


    mov esi,[ebp+8]
    push esi
    ;next_path
    mov ebx,ebp
    sub ebx,12288
    push ebx
    ;wcscpy_s(next_path, path);
    call crt_strcpy

    push offset slash
    mov ebx,ebp
    sub ebx,12288
    push ebx
    ;	wcscat_s(next_path, L"\\");
    call crt_strcat

    mov ebx,ebp
    sub ebx,12880
    add ebx,44
    push ebx

    mov ebx,ebp
    sub ebx,12288
    push ebx
    ;	wcscat_s(next_path, L"\\");
    call crt_strcat

    push [ebp + 12]
    mov ebx,ebp
    sub ebx,8192
    push ebx
    call crt_strcpy

    push offset prefix
    mov ebx,ebp
    sub ebx,8192
    push ebx
    call crt_strcat

    mov ebx,ebp
    sub ebx,8192
    push ebx

    mov ebx,ebp
    sub ebx,12288
    push ebx
    call dir_rec


    jmp do_while_end
    is_file:

    mov ebx,ebp
    sub ebx,12880
    add ebx,44
    push ebx

    push [ebp + 12]

    push offset file_print
    call crt_printf

    do_while_end:

    ;-------------------------------------------------- on appelle le FinFirstFile
    ;on stock handle
    mov eax,[ebp - 12884]

    ; on met le find_data dans ebp
    mov ebx,ebp
    sub ebx,12880

    ;handle = FindFirstFile(path_with_joker,&find_data)
    push ebx
    push eax
    call FindNextFile

    test eax,eax
    jne do_while
    ; on récupère l'état intial
    return :
    mov esp,ebp
    pop ebp
    ret

  ; ---------------------------- START ----------------------------------
  ; routine principale du programme de dir récursif
  start :

  push offset chemin
  call crt_printf

  push offset initial_path
  push offset string
  call crt_scanf

  ; on récupère une handle
  push offset prefix
  push offset initial_path
  call dir_rec

  ; on appelle la commande de pause afin de pouvoir voir la sortie du terminal
  push offset command
  call crt_system

  ; on appelle la fonction permettant de quitter le programme
  push 0
  call ExitProcess

  end start
