## Dir récursif - C

Auteur : Romain KRAFT <romain.kraft@protonmail.com>

## Pseudo Code

```c
fonction dir_recursif(CHAINE: repertoire, CHAINE: prefixe): VOID
  début
    pour chaque fichier dans (repertoire + "\\*") :
      si le fichier est un repertoire :
        afficher(prefixe,"Directory :", fichier)
        si  repertoire != "." et  repertoire != ".." :
          dir_recursif(repertoire + fichier, prefixe + "\t" )
      sinon :
        afficher(prefixe, "File :", fichier)
      fin si
    fin pour
  fin
```

## Implémentation en C

### Méthodes de recherche de fichiers/répertoires

Nous utiliserons les trois méthodes suivantes sous Windows afin de lister un répertoire :

1. La méthode [FindFirstFile](https://msdn.microsoft.com/en-us/library/windows/desktop/aa364418(v=vs.85).aspx) pour trouver le premier fichier du répertoire :

```c
HANDLE WINAPI FindFirstFile(
  _In_  LPCTSTR           lpFileName,
  _Out_ LPWIN32_FIND_DATA lpFindFileData
);
```

Cette méthode prend en entrée un `LPCTSTR`, on utilise donc des `wchar_t *` comme chaînes.
Le second argument est un `LPWIN32_FIND_DATA`, on utilise donc des `WIN32_FIND_DATA`.

L'argument `lpFileName` permet de renseigner le répertoire dans lequel lister les fichiers.

L'argument `lpFindFileData` est un objet qui aprés l'éxecution de la fonction contiendra toutes les informations concernant le premier fichier trouvé.

Enfin on récupère un `HANDLE` qui va nous permettre de savoir si la fonction à bien trouvé le répertoire/fichier, au cas contraire, sa valeur vaudra `INVALID_HANDLE_VALUE`.


2. La méthode [FindNextFile](https://msdn.microsoft.com/en-us/library/windows/desktop/aa364428(v=vs.85).aspx) qui va chercher le prochain fichier du répertoire :

```c

BOOL WINAPI FindNextFile(
  _In_  HANDLE            hFindFile,
  _Out_ LPWIN32_FIND_DATA lpFindFileData
);
```

Qui va prendre les mêmes arguments que `FindFirstFile` mais va renvoyer un `BOOL` en sorti afin de savoir si il a trouvé un fichier ou pas.

`FindNextFile`, si il a trouvé un fichier, va également mettre à jour `lpFindFileData` avec le fichier trouvé.

3. Enfin nous utiliserons [FindClose](https://msdn.microsoft.com/en-us/library/windows/desktop/aa364413(v=vs.85).aspx) afin de clore la recherche de fichier :

```c
BOOL WINAPI FindClose(
  _Inout_ HANDLE hFindFile
);
```
Le paramètre passé est la variable de type `HANDLE` que nous a renvoyé `FindFirstFile`.

### Méthodes de traitement des chaînes :

Les chaines étant du type `wchar_t *` n'étant pas des 'chaînes classiques' ( comme celles que l'ont utilise sous Linux par exemple ).

La définission d'une variable de type `wchar_t *` se fait en ajoutant `L` devant la déclaration d'une chaîne classique ( `""`)

Les méthodes suivantes seront utilisées :
1. `wcsncmp()` qui remplacera `strcmp()`
2. `wcsprintf()` qui remplacera `printf` ( à noter que le formatteur pour afficher un `wchar_t *` est `%ws` )
3. `wcscpy_s()` qui remplacera `strcpy_s()`
4. `wcscat_s()` qui remplacera `strcat_s()`
