// Author : Romain KRAFT <romain.kraft@protonmail.com>


// ConsoleApplication2.cpp : définit le point d'entrée pour l'application console.


#include "stdafx.h"
#include <windows.h>

#define MAXPATH 4096
#define PREFIX L"\t"

void dir_rec(wchar_t* argv, wchar_t* prefix);

int main(int argc, wchar_t** argv)
{
  // Prefix initial vide
  wchar_t initial_prefix[] = L"";
  // Chemin d'entrée en debug
  wchar_t test[] = L"C:\\Users\\Reverse\\Desktop\\x96 dbg";

	//if (argc != 2) {
		//wprintf(L"Usage : %S <chemin>", argv[0]);
		//Sleep(1000);
		//return 1;
	//}



	// A décommenter pour débugguer
	dir_rec(test, initial_prefix);

	//dir_rec(argv[1], initial_prefix);

	system("PAUSE");
	return 0;
}


void dir_rec(wchar_t * path,wchar_t* prefix) {

	// Structures permettant de faire la recherche de chemin
	WIN32_FIND_DATAW find_data;
	HANDLE handle;

	// Chemin courant sans le "\*"
	wchar_t path_with_joker[MAXPATH];

	// Variables qui vont servir à mettre à jour le chemin pour la prochaine recherche
	wchar_t next_path[MAXPATH];
	wchar_t next_prefix[MAXPATH];

	wcscpy_s(path_with_joker,path);
	wcscat_s(path_with_joker, L"\\*");

	// Affichage permettant le debug
	//wprintf(L"Currently in %ws directory\n", path_with_joker);

	// On cherche le premier fichier
	handle = FindFirstFileW(path_with_joker, &find_data);

	// Si le chemin est mauvais
	if (handle == INVALID_HANDLE_VALUE) {
		wprintf(L"Erreur lors de l'execution de FindFirstFile(\"%ws\",0x%p) ==> Erreur Code : %d\n", path, &find_data,GetLastError());
		return;
	}
	else {
		do {
			// Si le fichier est bien un répertoire on va vérifier que ce ne soit ni le répertoire "." et ".." pour éviter les boucles infinies
			if (find_data.dwFileAttributes == FILE_ATTRIBUTE_DIRECTORY) {

					// On log avec le prefix ( pour se repérer dans l'arborescence
					wprintf(L"%wsDirectory :%ws\n", prefix, find_data.cFileName);

					if (wcsncmp(find_data.cFileName, L".", MAXPATH) != 0 && wcsncmp(find_data.cFileName, L"..", MAXPATH)) {

						// Ici on va :
						// - Copier l'ancien chemin dans un nouveau puis lui rajouter le nom du répertoire afin de générer le prochain chemin
						// - Rajouter une étape au préfixe pour faciliter la compréhension de la sortie
						wcscpy_s(next_path, path);
						wcscat_s(next_path, L"\\");
						wcscat_s(next_path, MAX_PATH, find_data.cFileName);

						wcscpy_s(next_prefix, prefix);
						wcscat_s(next_prefix, MAXPATH, PREFIX);
						dir_rec(next_path, next_prefix);
					}
			}
			else
			{	// Si c'est un fichier on affiche le nom du fichier
				wprintf(L"%wsFile : %ws\n", prefix, find_data.cFileName);
			}
		// On fait ça tant qu'il y a un prochain fichier
		} while (FindNextFileW(handle, &find_data) != 0);

		// On ferme le handle afin d'éviter les fuites de mémoire
		FindClose(handle);
	}
}
