.386
.model flat,stdcall
option casemap:none

include c:\masm32\include\windows.inc
include c:\masm32\include\gdi32.inc
include c:\masm32\include\gdiplus.inc
include c:\masm32\include\user32.inc
include c:\masm32\include\kernel32.inc
include c:\masm32\include\msvcrt.inc

includelib c:\masm32\lib\gdi32.lib
includelib c:\masm32\lib\kernel32.lib
includelib c:\masm32\lib\user32.lib
includelib c:\masm32\lib\msvcrt.lib

WinMain PROTO :DWORD,:DWORD,:DWORD,:DWORD
EditWndProc PROTO :DWORD,:DWORD,:DWORD,:DWORD
PrintWndProc PROTO :DWORD,:DWORD,:DWORD,:DWORD

.data
ClassName  db "SubclassWinClass",0
AppName    db "Dir recursif",0
EditClass  db "EDIT",0
Message  db "Let's the dir begin !",0
; données définies par des #define dans l'implémentation en C
chemin db"Veuillez entrer un chemin :",10,0
max_path dword 256
prefix  db 9,0
;initial_path db"C:\Users\Reverse\Desktop\x96 dbg",0
slash_joker db "\*",0
slash db "\",0
dot db".",0
double_dot db"..",0
string db"%s",0
deci db"%d",0
new_line db"\r\n",0

; commandes
command db "PAUSE",0

; messages de format
error_handle db "Erreur lors de l'exécution de FindFirstFile",10,0
directory_print db "%sDirectory : %s",13,10,0
file_print db "%sFile : %s",13,10,0

; definition des variables non initialisées


.data?
hInstance  HINSTANCE ?
hwndEdit dd ?
hwndPrint dd ?
OldWndProc dd ?
editText dd ?

.code

dir_rec:
; on sauvegarde l'adresse de retour
push ebp
mov ebp,esp

; on alloue 8 de mémoire pour les variables locales
; - 4 octets pour HANDLE
; - 592 octets pour WIN32_FIND_DATA
; - 256 pour next_path
; - 256 pour next_prefix
; - 256 pour path_with_joker

; on a donc les variables aux adresse suivantes :
; &handle => ebp - 1264;1264
; &find_data => ebp - 1260;1260
; next_path => ebp - 768;768
; next_prefix => ebp - 512 ; 512
; path_with_joker => ebp - 256 ,4086 -3840
sub esp, 5208

;-------------------------------------------------- On créé le path_avec le joker

;	wcscpy_s(path_with_joker,path);

mov eax, [ebp+8]
push eax

mov eax,ebp
sub eax,256
push eax

call crt_strcpy


;wcscat_s(path_with_joker, L"\\*");
mov eax,ebp
sub eax,256
push offset slash_joker
push eax
call crt_strcat


;-------------------------------------------------- on appelle le FinFirstFile
;on stock path with joker
mov eax,ebp
sub eax,256

; on met le find_data dans ebp
mov ebx,ebp
sub ebx,1260

;handle = FindFirstFile(path_with_joker,&find_data)
push ebx
push eax
call FindFirstFile
mov [ebp - 1264],eax

;----------------------------------------------Condidition
;	if (handle == INVALID_HANDLE_VALUE) {
cmp eax, -1
jne handle_ok

; message erreur
;push offset error_handle
; wprintf(L"Erreur lors de l'execution de FindFirstFile(\"%ws\",0x%p) ==> Erreur Code : %d\n", path, &find_data,GetLastError());
mov esi, [ebp + 8]
push esi
call crt_printf

jmp return
;else{
handle_ok:

; DWORD    dwFileAttributes;   0
; FILETIME ftCreationTime;     4
; FILETIME ftLastAccessTime;   12
; FILETIME ftLastWriteTime;    20
; DWORD    nFileSizeHigh;      28
; DWORD    nFileSizeLow;       32
; DWORD    dwReserved0;        36
; DWORD    dwReserved1;        40
; TCHAR    cFileName[MAX_PATH];44
; TCHAR    cAlternateFileName[14];
do_while:
mov ebx,ebp
sub ebx,1260
mov ebx,[ebx]
cmp ebx,16
jne is_file

mov ebx,ebp
sub ebx,1260
add ebx,44
push ebx

push [ebp + 12]
push offset directory_print
mov ebx,ebp
sub ebx,5212
push ebx
call crt_sprintf


mov ebx,ebp
sub ebx,5212
push ebx
call print_text

;	if (wcsncmp(find_data.cFileName, L".", MAXPATH) != 0 && wcsncmp(find_data.cFileName, L"..", MAXPATH)) {
mov ebx,ebp
sub ebx,1260
add ebx,44
push ebx
push offset dot
call crt_strcmp
test eax,eax
je do_while_end

mov ebx,ebp
sub ebx,1260
add ebx,44
push ebx
push offset double_dot
call crt_strcmp
test eax,eax
je do_while_end

mov eax,[ebp+8]
push eax

;next_path
mov ebx,ebp
sub ebx,768
push ebx
;wcscpy_s(next_path, path);
call crt_strcpy

push offset slash
mov ebx,ebp
sub ebx,768
push ebx
;	wcscat_s(next_path, L"\\");
call crt_strcat

mov ebx,ebp
sub ebx,1260
add ebx,44
push ebx

mov ebx,ebp
sub ebx,768
push ebx
;	wcscat_s(next_path, L"\\");
call crt_strcat

push [ebp + 12]
mov ebx,ebp
sub ebx,512
push ebx
call crt_strcpy

push offset prefix
mov ebx,ebp
sub ebx,512
push ebx
call crt_strcat


mov ebx,ebp
sub ebx,512
push ebx

mov ebx,ebp
sub ebx,768
push ebx

call dir_rec


jmp do_while_end
is_file:

mov ebx,ebp
sub ebx,1260
add ebx,44
push ebx

push [ebp + 12]
push offset file_print
mov ebx,ebp
sub ebx,5212
push ebx
call crt_sprintf

mov ebx,ebp
sub ebx,5212
push ebx
call print_text

do_while_end:

;-------------------------------------------------- on appelle le FinFirstFile
;on stock handle
mov eax,[ebp - 1264]

; on met le find_data dans ebp
mov ebx,ebp
sub ebx,1260

;handle = FindFirstFile(path_with_joker,&find_data)
push ebx
push eax
call FindNextFile

test eax,eax
jne do_while
; on récupère l'état intial
return :
mov esp,ebp
pop ebp
ret

print_text:
; on sauvegarde l'adresse de retour
    push ebp
    mov ebp,esp


    mov eax,-1

    mov ebx,eax
    push 	hwndPrint
    call 	SetFocus

    push ebx
    push offset deci
    call crt_printf

    push 	ebx
    push 	ebx
    push 	EM_SETSEL
    push 	hwndPrint
    call 	SendMessage

    mov esi,[ebp+8]
    push esi
    push NULL
    push EM_REPLACESEL
    push hwndPrint
    call SendMessage
    mov esp,ebp
    pop ebp
    ret

start:
    invoke GetModuleHandle, NULL
    mov    hInstance,eax
    invoke WinMain, hInstance,NULL,NULL, SW_SHOWDEFAULT
    invoke ExitProcess,eax

WinMain proc hInst:HINSTANCE,hPrevInst:HINSTANCE,CmdLine:LPSTR,CmdShow:DWORD
    LOCAL wc:WNDCLASSEX
    LOCAL msg:MSG
    LOCAL hwnd:HWND
    mov   wc.cbSize,SIZEOF WNDCLASSEX
    mov   wc.style, CS_HREDRAW or CS_VREDRAW
    mov   wc.lpfnWndProc, OFFSET WndProc
    mov   wc.cbClsExtra,NULL
    mov   wc.cbWndExtra,NULL
    push  hInst
    pop   wc.hInstance
    mov   wc.hbrBackground,COLOR_APPWORKSPACE
    mov   wc.lpszMenuName,NULL
    mov   wc.lpszClassName,OFFSET ClassName
    invoke LoadIcon,NULL,IDI_APPLICATION
    mov   wc.hIcon,eax
    mov   wc.hIconSm,eax
    invoke LoadCursor,NULL,IDC_ARROW
    mov   wc.hCursor,eax
    invoke RegisterClassEx, addr wc
    invoke CreateWindowEx,WS_EX_CLIENTEDGE,ADDR ClassName,ADDR AppName,\
 WS_OVERLAPPED+WS_CAPTION+WS_SYSMENU+WS_MINIMIZEBOX+WS_MAXIMIZEBOX+WS_VISIBLE,CW_USEDEFAULT,\
           CW_USEDEFAULT,800,800,NULL,NULL,\
           hInst,NULL
    mov   hwnd,eax
    .while TRUE
        invoke GetMessage, ADDR msg,NULL,0,0
        .BREAK .IF (!eax)
        invoke TranslateMessage, ADDR msg
        invoke DispatchMessage, ADDR msg
    .endw
    mov eax,msg.wParam
    ret
WinMain endp

WndProc proc hWnd:HWND, uMsg:UINT, wParam:WPARAM, lParam:LPARAM
    .if uMsg==WM_CREATE
        invoke CreateWindowEx,WS_EX_CLIENTEDGE,ADDR EditClass,NULL,\
            WS_CHILD+WS_VISIBLE+WS_BORDER+ES_AUTOHSCROLL,20,\
            20,760,25,hWnd,NULL,\
            hInstance,NULL
        mov hwndEdit,eax
        invoke SetFocus,eax
        ;-----------------------------------------
        ; Subclass it!
        ;-----------------------------------------
        invoke SetWindowLong,hwndEdit,GWL_WNDPROC,addr EditWndProc
        mov OldWndProc,eax


        ;; On créé notre fenêtre pour print
        invoke CreateWindowEx,WS_EX_CLIENTEDGE,ADDR EditClass,NULL,\
        WS_CHILD + WS_VISIBLE + WS_BORDER + ES_LEFT +\
              ES_AUTOHSCROLL + WS_HSCROLL + WS_VSCROLL + \
              ES_MULTILINE + ES_READONLY ,20,\
            65,760,600,hWnd,NULL,\
            hInstance,NULL
        mov hwndPrint,eax
        ;-----------------------------------------
        ; Subclass it!
        ;-----------------------------------------
        invoke SetWindowLong,hwndPrint,GWL_WNDPROC,addr PrintWndProc
        mov OldWndProc,eax
    .elseif uMsg==WM_DESTROY
        invoke PostQuitMessage,NULL
    .else
        invoke DefWindowProc,hWnd,uMsg,wParam,lParam
        ret
    .endif
    xor eax,eax
    ret
WndProc endp

EditWndProc PROC hEdit:DWORD,uMsg:DWORD,wParam:DWORD,lParam:DWORD
    .if uMsg==WM_KEYDOWN
        mov eax,wParam
        .if al==VK_RETURN
            invoke GetWindowText,hwndEdit,ADDR editText,512

            push offset prefix
            push offset editText
            call dir_rec
        .else
            invoke CallWindowProc,OldWndProc,hEdit,uMsg,wParam,lParam
            ret
        .endif
    .else
        invoke CallWindowProc,OldWndProc,hEdit,uMsg,wParam,lParam
        ret
    .endif
    xor eax,eax
    ret
EditWndProc endp

PrintWndProc PROC hEdit:DWORD,uMsg:DWORD,wParam:DWORD,lParam:DWORD
  invoke CallWindowProc,OldWndProc,hEdit,uMsg,wParam,lParam
  xor eax,eax
  ret
PrintWndProc endp
end start
