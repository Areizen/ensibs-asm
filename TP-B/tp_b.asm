.386
.model flat,stdcall
option casemap:none

include c:\masm32\include\windows.inc
include c:\masm32\include\gdi32.inc
include c:\masm32\include\gdiplus.inc
include c:\masm32\include\user32.inc
include c:\masm32\include\kernel32.inc
include c:\masm32\include\msvcrt.inc

includelib c:\masm32\lib\gdi32.lib
includelib c:\masm32\lib\kernel32.lib
includelib c:\masm32\lib\user32.lib
includelib c:\masm32\lib\msvcrt.lib

.data
  chaine db"ce texte va passer en majuscule",10,0
  len_chaine db"La longueur de la chaine est de %d",10,0

.code

  len_count:
    ;sauvegarde de l'adresse de retour
    push ebp
    mov ebp,esp

    ;chargement de la chaine
    mov edi, [ebp+8]

    xor eax,eax
    ;on sauvegarde le pointeur initiale de la chaîne
    mov ebx,edi

    ;on va scanner de façon repété un byte par un byte jusqu'a tomber sur un NULL byte
    repne scasb

    ;on soustraite le pointeur sur le dernier caractère de la chaine avec le pointeur sur le premier
    sub edi,ebx

    ; on met le retour dans eax
    mov eax,edi
    ;on mov esp,ebp
    mov esp, ebp
    ;on redefinie l'adresse de retour
    pop ebp
    ;on retourne avec le résultat stocké dans eax
    ret

  uppercase:
    ;sauvegarde de l'adresse de retour
    push ebp
    mov ebp,esp

    ; chargement de la chaîne de caractère
    mov esi, [ebp+8]

    ;tant que le pointeur courant sur la chaine n'est pas égale à un NULLBYTE
    ; si le caractère courant est supérieur à 96 et le inférieur à 123
    ;   on soustrait 32 au caractère et on le sauvegarde dans la cahine
    ;  on incrémente le pointeur sur la chaine
    for_loop:
    mov al,[esi]
    cmp al,123
    jg end_of_the_loop

    cmp al,96
    jl end_of_the_loop

    sub al,32
    mov [esi],al

    end_of_the_loop:
    inc esi
    test al,al

    jnz for_loop


    ;on mov esp,ebp
    mov esp, ebp
    pop ebp
    ret

  start:

    push offset chaine
    call len_count

    push eax
    push offset len_chaine
    call crt_printf

    push offset chaine
    call uppercase

    push offset chaine
    call crt_printf

    push 0
    call ExitProcess



  end start
