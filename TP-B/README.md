# Rapport de TP-B ENSIBS-ASM
Auteur : Romain KRAFT <romain.kraft@protonmail.com>

## Première partie: Mettre en majuscule une chaîne

### Pseudo Code :
```c
fonction uppercase(CHAINE message:INOUT)
  debut
    pour i dans message :
      si i => 'a' ou i <= 'z' :
         i = i - 32
  fin
```

### Code ASM :

```masm32
uppercase:
  ;sauvegarde de l'adresse de retour
  push ebp
  mov ebp,esp

  ; chargement de la chaîne de caractère
  mov esi, [ebp+8]

  ;tant que le pointeur courant sur la chaine n'est pas égale à un NULLBYTE
  ; si le caractère courant est supérieur à 96 et le inférieur à 123
  ;   on soustrait 32 au caractère et on le sauvegarde dans la cahine
  ;  on incrémente le pointeur sur la chaine
  for_loop:
  mov al,[esi]
  cmp al,123
  jg end_of_the_loop

  cmp al,96
  jl end_of_the_loop

  sub al,32
  mov [esi],al

  end_of_the_loop:
  inc esi
  test al,al

  jnz for_loop


  ;on mov esp,ebp
  mov esp, ebp
  pop ebp
  ret
```

### Optimisations :

Dans cette fonction je n'ai pas fait d'optimisation particulière.

À noter que j'utilise `al` pour stocker un caractère, puisque un caractère est défini sur un *byte*.
J'utilise également `test eax,eax` plutôt que `cmp eax,0` puisque l'instruction `test` est plus rapide que l'instruction `cmp`.

## Seconde partie : calcule de la longueur d'une chaîne :

### Pseudo Code :

```c
fonction lencount(CHAINE:message:INOUT):
  debut
    i = offset_debut(message)
    i_current = i

    tant que message[i] != '\x00' :
      i_current = i_current + 1

    retourne i_current-i
  fin
```
### Code ASM:
```masm32
len_count:
  ;sauvegarde de l'adresse de retour
  push ebp
  mov ebp,esp

  ;chargement de la chaine
  mov edi, [ebp+8]

  xor eax,eax
  ;on sauvegarde le pointeur initiale de la chaîne
  mov ebx,edi

  ;on va scanner de façon repété un byte par un byte jusqu'a tomber sur un NULL byte
  repne scasb

  ;on soustraite le pointeur sur le dernier caractère de la chaine avec le pointeur sur le premier
  sub edi,ebx

  ; on met le retour dans eax
  mov eax,edi
  ;on mov esp,ebp
  mov esp, ebp
  ;on redefinie l'adresse de retour
  pop ebp
  ;on retourne avec le résultat stocké dans eax
  ret
```

### Optimisations:

Ici la grosse optimisation est l'instructions `repne scasb`, en effet cette instruction va faire une boucle sur `edi` jusqu'à ce que la valeur pointée par `edi` soit un `NULLBYTE`.

On sauvegarde juste la valeur du début de la chaîne et on récupère celle de la fin aprés avoir executé `repne scasb`. Une fois ces opérations faites on soustrait le tout pour avoir la longueur de la chaîne.
