.386
.model flat,stdcall
option casemap:none

include c:\masm32\include\windows.inc
include c:\masm32\include\gdi32.inc
include c:\masm32\include\gdiplus.inc
include c:\masm32\include\user32.inc
include c:\masm32\include\kernel32.inc
include c:\masm32\include\msvcrt.inc

includelib c:\masm32\lib\gdi32.lib
includelib c:\masm32\lib\kernel32.lib
includelib c:\masm32\lib\user32.lib
includelib c:\masm32\lib\msvcrt.lib

.data
  chaine db"Le resultat pour %d est : %d",10,0
  chaine_test db"ceci est un test pour compter le nombre de caractère valant a,b,c",10,0
  message db"La chaine '%s' contient %d 'a', %d 'b', %d 'c'",10,0
.code

  ;ca pue le fibonacci
  myst :
    ; sauvegarde de l'adresse de retour
    push ebp
    mov ebp,esp

    ; on reserve la place pour 4 entier de 32 bits
    sub esp,16

    ; i = ebp-4 , j = ebp-8 , k = ebp-12 , l = ebp-16
    ; on initialise i,j,k,l
    mov ebx,3
    mov [ebp-4],ebx
    mov ebx,1
    mov [ebp-8],ebx
    mov ebx,1
    mov [ebp-12],ebx

    mov ecx,[ebp-4]
    for_1:
      ; l = j + k
      mov ebx,[ebp-8]
      add ebx,[ebp-12]
      mov [ebp-16],ebx

      ; j = k
      mov ebx,[ebp-12]
      mov [ebp-8],ebx

      ; k = l
      mov ebx,[ebp-16]
      mov [ebp-12], ebx

      ;on incremente ecx
      inc ecx

      ;on load n
      mov eax,[ebp+8]

      ; on fait
      sub eax,ecx

      cmp eax,0
      jnl for_1

    mov eax,[ebp-12]

    mov esp,ebp
    pop ebp
    ret

  count_abc:
    ; sauvegarde de l'adresse de retour
    push ebp
    mov ebp,esp

    sub esp,12

    ; a=[ebp-12];b=[ebp-8];c=[ebp-4] et a,b,c=0
    xor ebx,ebx
    mov [ebp-12],ebx
    mov [ebp-8],ebx
    mov [ebp-4],ebx

    mov esi,[ebp+8]
    mov ecx,1
    for_2:
      mov al,[esi]
      test_a:
        ;on compare al à "a"
        cmp al,97
        jne test_b
        add [ebp-12],ecx
        jmp end_for
      test_b:
        ;on compare al à "b"
        cmp al,98
        jne test_c
        add [ebp-8],ecx
        jmp end_for
      test_c:
        ;on compare al à "c"
        cmp al,99
        jne end_for
        add [ebp-4],ecx
        jmp end_for
      end_for:
      inc esi
      cmp al,0
      jne for_2

    mov eax,[ebp-12]
    mov ebx,[ebp-8]
    mov ecx,[ebp-4]

    mov esp,ebp
    pop ebp
    ret

  start:
    push 10
    call myst

    push eax
    push 10
    push offset chaine
    call crt_printf

    push offset chaine_test
    call count_abc

    push ecx
    push ebx
    push eax
    push offset chaine_test
    push offset message
    call crt_printf

    push 0
    call ExitProcess
  end start
