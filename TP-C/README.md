# Rapport de TP-C ENSIBS-ASM #
Auteur : Romain KRAFT <romain.kraft@protonmail.com>

## Première Partie : Fonction Mystèrieuse ##
### Code a reproduire en assembleur :
 ```c
int myst( int n ){
  int i, j, k, l;
  j = 1;
  k = 1;
  for ( i = 3; i <= n; i++ ) {
    l = j + k;
    j = k;
    k = l;
  }
  return k;
}
 ```

### Code ASM :
```masm32
myst :
  ; sauvegarde de l'adresse de retour
  push ebp
  mov ebp,esp

  ; on reserve la place pour 4 entier de 32 bits
  sub esp,16

  ; i = ebp-4 , j = ebp-8 , k = ebp-12 , l = ebp-16
  ; on initialise i,j,k,l
  mov ebx,3
  mov [ebp-4],ebx
  mov ebx,1
  mov [ebp-8],ebx
  mov ebx,1
  mov [ebp-12],ebx

  mov ecx,[ebp-4]
  for_1:
    ; l = j + k
    mov ebx,[ebp-8]
    add ebx,[ebp-12]
    mov [ebp-16],ebx

    ; j = k
    mov ebx,[ebp-12]
    mov [ebp-8],ebx

    ; k = l
    mov ebx,[ebp-16]
    mov [ebp-12], ebx

    ;on incremente ecx
    inc ecx

    ;on load n
    mov eax,[ebp+8]

    ; on fait
    sub eax,ecx

    cmp eax,0
    jnl for_1

  mov eax,[ebp-12]

  mov esp,ebp
  pop ebp
  ret
```

### Explications : ###

1. L'algorithme que l'on programme ici est un algorithme qui génère le résultat d'une suite de **Fibonacci**

2. Ici on utilise les variables locales, comme on a 4 entier, on décremente `esp` de `4*sizeof(int)` soit `sub esp,16`

On a donc notre stack qui ressemble à ça :

<br/>
<center>
<img width="60%" src="./assets/local_variable.png" />
</center>
<br/>

Une fois cette partie effectuée, le reste est effectué via une boucle et des opérations basiques.

## Seconde partie : compteur de caractères a,b et c

### Pseudo Code :

```c
fonction count_abc(CHAINE message):ENTIER,ENTIER,ENTIER
  debut
    a, b, c = 0
    pour i dans message :
      si i = "a" :
        a = a + 1
      sinon si i = "b" :
        b = b + 1
      sinon si i = "c" :
        c = c + 1
      fin si
    retourne a,b,c
  fin
```
<br/>

### Code ASM :

```masm32
count_abc:
  ; sauvegarde de l'adresse de retour
  push ebp
  mov ebp,esp

  sub esp,12

  ; a=[ebp-12];b=[ebp-8];c=[ebp-4] et a,b,c=0
  xor ebx,ebx
  mov [ebp-12],ebx
  mov [ebp-8],ebx
  mov [ebp-4],ebx

  mov esi,[ebp+8]
  mov ecx,1
  for_2:
    mov al,[esi]
    test_a:
      ;on compare al à "a"
      cmp al,97
      jne test_b
      add [ebp-12],ecx
      jmp end_for
    test_b:
      ;on compare al à "b"
      cmp al,98
      jne test_c
      add [ebp-8],ecx
      jmp end_for
    test_c:
      ;on compare al à "c"
      cmp al,99
      jne end_for
      add [ebp-4],ecx
      jmp end_for
    end_for:
    inc esi
    cmp al,0
    jne for_2

  mov eax,[ebp-12]
  mov ebx,[ebp-8]
  mov ecx,[ebp-4]

  mov esp,ebp
  pop ebp
  ret
```

### Explications : ###

Ici on utilise une boucle comme celle utilisé dans le [TP A](../TP-A/README.md).

La seule vraie différence est qu'on initialise 3 variables locales qui nous servent de compteur pour les occurences de chacun des caractère.

On a donc une stack semblable à la suivante :

<br/>
<center>
  <img width="60%" src="./assets/local_variable2.png">
</center>
