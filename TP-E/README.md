# Rapport de TP-D ENSIBS-ASM

Auteur : Romain KRAFT <romain.kraft@protonmail.com>

## Explication "succincte"

### Userland et Kerneland :
<center>
  <img width="60%" src="./assets/rings.png">
</center>

<br/>

Dans Windows, il existe plusieurs type d'instructions, celles accessibles par les utilisateurs, dans le *Userland* avec des droits **Ring 3**.

Celles accessibles uniquement avec des droits **Ring 0** dans le *Kernelland* .

### Comment appeller les instructions kernel :

Pour transiter dans le kernel, l'utilisateur appelle l'instruction `kiSystemCall` qui prend en paramètre `eax` l'index de la fonction à appeler.

Ensuite l'instruction `sysenter` va faire la transition dans le Kernel, cette instruction utilise des registre `MSR` qui sont appelable via les instructions `rdmsr` et `wrmsr`.

Le passage est fait en `Kerneland`.Arrivé ici `KiFastCallEntry` est appelé et va trouver la fonction associée à l'index et l'exécuter.

Pour finir un appel est fait à `KiServiceExit` qui va appeler `kiSystemCallExit` pour retourner en Userland.
