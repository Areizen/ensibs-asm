.386
.model flat,stdcall
option casemap:none

include c:\masm32\include\windows.inc
include c:\masm32\include\gdi32.inc
include c:\masm32\include\gdiplus.inc
include c:\masm32\include\user32.inc
include c:\masm32\include\kernel32.inc
include c:\masm32\include\msvcrt.inc

includelib c:\masm32\lib\gdi32.lib
includelib c:\masm32\lib\kernel32.lib
includelib c:\masm32\lib\user32.lib
includelib c:\masm32\lib\msvcrt.lib

.data
  message db"Ceci est un message",10,0
  caption db"Ceci est une messageBox",10,0

.code
  start:
    push MB_OK
    push offset caption
    push offset message
    push 0
    call MessageBox

    push 0
    call ExitProcess

  end start
